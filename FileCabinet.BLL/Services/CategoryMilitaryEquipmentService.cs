﻿using AutoMapper;
using FileCabinet.BLL.Interfaces;
using FileCabinet.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;
using FileCabinet.DAL.Entities;

namespace FileCabinet.BLL.Services
{
    public class CategoryMilitaryEquipmentService : ICategoryMilitaryEquipment
    {
        private readonly IUnitOfWork _dataBase;
        private readonly IMapper _mapper;
        public CategoryMilitaryEquipmentService(IUnitOfWork db)
        {
            _dataBase = db;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipment, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<Country, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmy, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }
        public void Dispose()
        {
            _dataBase.Dispose();
        }
        public async Task<IEnumerable<CategoryMilitaryEquipmentDTO>> GetAll()
        {
            var data = _mapper.Map<IEnumerable<CategoryMilitaryEquipment>, IEnumerable<CategoryMilitaryEquipmentDTO>>(await _dataBase.CategoryMilitaryEquipments.GetAll());
            return data;
        }
        public void Add(CategoryMilitaryEquipmentDTO dto)
        {
            var newdto = _mapper.Map<CategoryMilitaryEquipmentDTO, CategoryMilitaryEquipment>(dto);
            _dataBase.CategoryMilitaryEquipments.Create(newdto);
        }

        public async Task Delete(CategoryMilitaryEquipmentDTO dto)
        {
            await _dataBase.CategoryMilitaryEquipments.Delete(dto.CategoryMilitaryEquipmentID);
        }

        public CategoryMilitaryEquipmentDTO ConfimDelete(int? id)
        {
            var deletedCategory = _dataBase.CategoryMilitaryEquipments.Find(s => s.CategoryMilitaryEquipmentID == id).FirstOrDefault();
            var result = _mapper.Map<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>(deletedCategory);
            return result;
        }

        public void Update(CategoryMilitaryEquipmentDTO dto)
        {
            var result = _mapper.Map<CategoryMilitaryEquipmentDTO, CategoryMilitaryEquipment>(dto);
            _dataBase.CategoryMilitaryEquipments.Update(result);
        }

        public CategoryMilitaryEquipmentDTO Details(int? id)
        {
            var query = _dataBase.CategoryMilitaryEquipments.Find(c => c.CategoryMilitaryEquipmentID == id).FirstOrDefault();
            var result = _mapper.Map<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>(query);
            return result;
        }

        /// <summary>
        /// Gets all categories and adds an empty one for the filter
        /// </summary>
        /// <returns>
        /// Returns a list of categories for a droplist
        /// </returns>
        public async Task<IEnumerable<CategoryMilitaryEquipmentDTO>> GetAllForSelectList()
        {
            var data = _mapper.Map<IEnumerable<CategoryMilitaryEquipment>, List<CategoryMilitaryEquipmentDTO>>(await _dataBase.CategoryMilitaryEquipments.GetAll());
            data.Insert(0, new CategoryMilitaryEquipmentDTO { Name = "All", CategoryMilitaryEquipmentID = 0 });

            return data;
        }

        /// <summary>
        /// Checks if a similar category is in the database when adding or editing
        /// </summary>
        /// <returns>
        /// Returns the boolean result of finding a country in the database
        /// </returns>
        public async Task<bool> CheckIfExistsCategory(CategoryMilitaryEquipmentDTO dto)
        {
            var model = _mapper.Map<IEnumerable<CategoryMilitaryEquipment>, IEnumerable<CategoryMilitaryEquipmentDTO>>(await _dataBase.CategoryMilitaryEquipments.GetAll());
            var existInDb = model.Any(it => it.Name == dto.Name);

            var selectAll = model.Where(it => it.Name == dto.Name);
            var edit = selectAll.Any(it => it.CategoryMilitaryEquipmentID == dto.CategoryMilitaryEquipmentID);

            if (edit == true)
            {
                existInDb = false;
            }

            return existInDb;
        }
    }
}
