﻿using AutoMapper;
using FileCabinet.BLL.Interfaces;
using FileCabinet.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;
using FileCabinet.DAL.Entities;

namespace FileCabinet.BLL.Services
{
    public class MilitaryEquipmentService : IMilitaryEquipmentService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public MilitaryEquipmentService(IUnitOfWork db)
        {
            _database = db;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipment, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<Country, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmy, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }

        public async Task<IEnumerable<MilitaryEquipmentDTO>> GetAllEquipment()
        {
            var equipment = _mapper.Map<IEnumerable<MilitaryEquipment>, IEnumerable<MilitaryEquipmentDTO>>(await _database.MilitaryEquipments.GetAll());
            return equipment;
        }

        public void Add(MilitaryEquipmentDTO dto)
        {
            var newdto = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipment>(dto);
            _database.MilitaryEquipments.Create(newdto);
        }

        public async Task Delete(MilitaryEquipmentDTO dto)
        {
            await _database.MilitaryEquipments.Delete(dto.MilitaryEquipmentID);
        }

        public MilitaryEquipmentDTO ConfimDelete(int? id)
        {
            var x = _database.MilitaryEquipments.Find(s => s.MilitaryEquipmentID == id).FirstOrDefault();
            var result = _mapper.Map<MilitaryEquipment, MilitaryEquipmentDTO>(x);
            return result;
        }

        public void Update(MilitaryEquipmentDTO dto)
        {
            var result = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipment>(dto);
            _database.MilitaryEquipments.Update(result);
        }

        public MilitaryEquipmentDTO Details(int? id)
        {
            var selectedMilitaryEquipment = _database.MilitaryEquipments.Find(c => c.MilitaryEquipmentID == id).First();
            var result = _mapper.Map<MilitaryEquipment, MilitaryEquipmentDTO>(selectedMilitaryEquipment);
            return result;
        }

        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Selects countries where there is a selected army category
        /// </summary>
        /// <param name="id"> selected type army </param>
        /// <returns>
        /// Returns the countries where the selected army category is
        /// </returns>
        public async Task<IEnumerable<CountryDTO>> SelectTypeArmy(int? id)
        {
            var countries = await _database.MilitaryEquipments.SelectTypeArmy(id);
            var result = _mapper.Map<IEnumerable<Country>, IEnumerable<CountryDTO>>(countries);
            return result;
        }

        /// <summary>
        /// Selects the categories that the country has
        /// </summary>
        /// <param name="CountryID"> selected country </param>
        /// <param name="TypeArmyID"> selected type army </param>
        /// <returns>
        /// Returns the categories that the selected country has
        /// </returns>
        public async Task<IEnumerable<CategoryMilitaryEquipmentDTO>> SelectCountryGetCategory(int CountryID, int TypeArmyID)
        {
            var selectedCategory = await _database.MilitaryEquipments.SelectCountryGetCategory(CountryID, TypeArmyID);
            var result = _mapper.Map<IEnumerable<CategoryMilitaryEquipment>, IEnumerable<CategoryMilitaryEquipmentDTO>>(selectedCategory);
            return result;
        }

        /// <summary>
        /// Selects user-selected military vehicles
        /// </summary>
        /// <param name="CategoryID"> selected category </param>
        /// <param name="TypeArmyID"> selected type army </param>
        /// <param name="CountryID"> selected country </param>
        /// <returns>
        /// Returns selected military equipment
        /// </returns>
        public async Task<IEnumerable<MilitaryEquipmentDTO>> SelectCategoryGetMilitaryEquipment(int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            var selectedMilitaryEquipment = await _database.MilitaryEquipments.SelectCategoryGetMilitaryEquipment(CategoryID, TypeArmyID, CountryID);
            var result = _mapper.Map<IEnumerable<MilitaryEquipment>, IEnumerable<MilitaryEquipmentDTO>>(selectedMilitaryEquipment);
            return result;
        }

        /// <summary>
        /// Filters military equipment according to the criteria set by the user
        /// </summary>
        /// <param name="searchModel"> the model in which the criteria for filtering data are located </param>
        /// <returns>
        /// Returns filtered data by criteria
        /// </returns>
        public IEnumerable<MilitaryEquipmentDTO> GetSortedEquipment(EquipmentSearchModelDTO searchModel)
        {
            var query = _database.MilitaryEquipments.GetAsQueryableResult();
            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.SearchString))
                    query = query.Where(x => x.Name.Contains(searchModel.SearchString));
                if (searchModel.TypeArmyID.HasValue && searchModel.TypeArmyID != 0)
                    query = query.Where(x => x.TypeArmyID == searchModel.TypeArmyID);
                if (searchModel.CountryID.HasValue && searchModel.CountryID != 0)
                    query = query.Where(x => x.CountryID == searchModel.CountryID);
                if (searchModel.CategoryMilitaryEquipmentID.HasValue && searchModel.CategoryMilitaryEquipmentID != 0)
                    query = query.Where(x => x.CategoryMilitaryEquipmentID == searchModel.CategoryMilitaryEquipmentID);

            }

            query = query.OrderByDescending(x => x.Name).ThenByDescending(x => x.CountryID);

            var result = _mapper.Map<IEnumerable<MilitaryEquipment>, IEnumerable<MilitaryEquipmentDTO>>(query);
            return result;
        }


        /// <summary>
        /// Checks if a similar military vehicles is in the database when adding or editing
        /// </summary>
        /// <returns>
        /// Returns the boolean result of finding a military vehicles in the database
        /// </returns>
        public async Task<bool> CheckIfExistsMilitaryEquipment(MilitaryEquipmentDTO dto)
        {
            var model = _mapper.Map<IEnumerable<MilitaryEquipment>, IEnumerable<MilitaryEquipmentDTO>>(await _database.MilitaryEquipments.GetAll());
            var existInDb = model.Any(it => it.Name == dto.Name);

            var selectAll = model.Where(it => it.Name == dto.Name);
            var edit = selectAll.Any(it => it.MilitaryEquipmentID == dto.MilitaryEquipmentID);

            if (edit == true)
            {
                existInDb = false;
            }

            return existInDb;
        }
    }
}
