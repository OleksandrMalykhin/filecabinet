﻿using AutoMapper;
using FileCabinet.BLL.Interfaces;
using FileCabinet.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;
using FileCabinet.DAL.Entities;

namespace FileCabinet.BLL.Services
{
    public class TypeArmyService : ITypeArmyService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public TypeArmyService(IUnitOfWork db)
        {
            _database = db;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipment, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<Country, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmy, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }

        public async Task<IEnumerable<TypeArmyDTO>> GetAll()
        {
            var all_news = _mapper.Map<IEnumerable<TypeArmy>, IEnumerable<TypeArmyDTO>>(await _database.TypeArmies.GetAll());
            return all_news;
        }

        public void Dispose()
        {
            _database.Dispose();
        }

        public void Add(TypeArmyDTO dto)
        {
            var newdto = _mapper.Map<TypeArmyDTO, TypeArmy>(dto);
            _database.TypeArmies.Create(newdto);
        }

        public async Task Delete(TypeArmyDTO dto)
        {
            await _database.TypeArmies.Delete(dto.TypeArmyID);
        }

        public TypeArmyDTO ConfimDelete(int? id)
        {
            var x = _database.TypeArmies.Find(s => s.TypeArmyID == id).FirstOrDefault();
            var result = _mapper.Map<TypeArmy, TypeArmyDTO>(x);
            return result;
        }

        public void Update(TypeArmyDTO dto)
        {
            var result = _mapper.Map<TypeArmyDTO, TypeArmy>(dto);
            _database.TypeArmies.Update(result);
        }

        /// <summary>
        /// Gets all type armies and adds an empty one for the filter
        /// </summary>
        /// <returns>
        /// Returns a list of type armies for a droplist
        /// </returns>
        public async Task<IEnumerable<TypeArmyDTO>> GetAllForSelectList()
        {
            var all_news = _mapper.Map<IEnumerable<TypeArmy>, List<TypeArmyDTO>>(await _database.TypeArmies.GetAll());
            all_news.Insert(0, new TypeArmyDTO { Name = "All", TypeArmyID = 0 });
            return all_news;
        }

        /// <summary>
        /// Checks if a similar type army is in the database when adding or editing
        /// </summary>
        /// <returns>
        /// Returns the boolean result of finding a type army in the database
        /// </returns>
        public async Task<bool> CheckIfExistsTypeArmy(TypeArmyDTO typeArmy)
        {
            var model = _mapper.Map<IEnumerable<TypeArmy>, IEnumerable<TypeArmyDTO>>(await _database.TypeArmies.GetAll());
            var existInDb = model.Any(it => it.Name == typeArmy.Name);

            var selectAll = model.Where(it => it.Name == typeArmy.Name);
            var edit = selectAll.Any(it => it.TypeArmyID == typeArmy.TypeArmyID);

            if (edit == true)
            {
                existInDb = false;
            }

            return existInDb;
        }
    }
}
