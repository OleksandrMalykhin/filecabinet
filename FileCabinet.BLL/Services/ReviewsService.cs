﻿using AutoMapper;
using FileCabinet.BLL.Interfaces;
using FileCabinet.DAL.Interfaces;
using System.Linq;
using FileCabinet.BLL.DTO;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.IdentityEntities;

namespace FileCabinet.BLL.Services
{
    public class ReviewsService : IReviewsService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public ReviewsService(IUnitOfWork db)
        {
            _database = db;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipment, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<Country, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmy, TypeArmyDTO>().ReverseMap();
                cfg.CreateMap<Reviews, ReviewsDTO>().ReverseMap();
                cfg.CreateMap<ApplicationUser, UserDTO>().ReverseMap();

            }).CreateMapper();
        }

        public void AddRating(ReviewsDTO dto)
        {
            var result = _mapper.Map<ReviewsDTO, Reviews>(dto);
            _database.Reviews.Create(result);
        }

        public void Dispose()
        {
            _database.Dispose();
        }

        public void EditReview(ReviewsDTO dto)
        {
            var result = _mapper.Map<ReviewsDTO, Reviews>(dto);
            _database.Reviews.Update(result);
        }

        public ReviewsDTO FindReview(string userid, int? militaryeqipm)
        {
            var query = _database.Reviews.Find(p => p.UserProfileId == userid).FirstOrDefault(p => p.MilitaryEquipmentID == militaryeqipm);
            var result = _mapper.Map<Reviews, ReviewsDTO>(query);
            return result;
        }
    }
}
