﻿using FileCabinet.BLL.Interfaces;
using FileCabinet.DAL.Interfaces;

namespace FileCabinet.BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IUserService CreateUserService(string connection)
        {
            return new UserService(new IdentityUnitOfWork(connection));
        }
    }
}
