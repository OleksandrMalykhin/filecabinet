﻿using AutoMapper;
using FileCabinet.BLL.Interfaces;
using FileCabinet.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;
using FileCabinet.DAL.Entities;

namespace FileCabinet.BLL.Services
{
    public class CountryService : ICountryService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        public CountryService(IUnitOfWork db)
        {
            _database = db;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipment, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<Country, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipment, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmy, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }

        public void Dispose()
        {
            _database.Dispose();
        }

        public async Task <IEnumerable<CountryDTO>> GetAll()
        {
            var data = _mapper.Map<IEnumerable<Country>, IEnumerable<CountryDTO>>(await _database.Countries.GetAll());
            return data;
        }

        public void Add(CountryDTO dto)
        {
            var newdto = _mapper.Map<CountryDTO, Country>(dto);
            _database.Countries.Create(newdto);
        }

        public async Task Delete(CountryDTO dto)
        {
            await _database.Countries.Delete(dto.CountryID);
        }

        public CountryDTO ConfimDelete(int? id)
        {
            var x = _database.Countries.Find(s => s.CountryID == id).FirstOrDefault();
            var result = _mapper.Map<Country, CountryDTO>(x);
            return result;
        }

        public void Update(CountryDTO dto)
        {
            var result = _mapper.Map<CountryDTO, Country>(dto);
            _database.Countries.Update(result);
        }

        public CountryDTO Details(int? id)
        {
            var result = _mapper.Map<Country, CountryDTO>(_database.Countries.Find(c => c.CountryID == id).First());
            return result;
        }

        /// <summary>
        /// Gets all countries and adds an empty one for the filter
        /// </summary>
        /// <returns>
        /// Returns a list of countries for a droplist
        /// </returns>
        public async Task<IEnumerable<CountryDTO>> GetAllForSelectList()
        {
            var data = _mapper.Map<IEnumerable<Country>, List<CountryDTO>>(await _database.Countries.GetAll());
            data.Insert(0, new CountryDTO { Name = "All", CountryID = 0 });
            return data;
        }

        /// <summary>
        /// Checks if a similar country is in the database when adding or editing
        /// </summary>
        /// <returns>
        /// Returns the boolean result of finding a category in the database
        /// </returns>
        public async Task<bool> CheckIfExistsCountry(CountryDTO dto)
        {
            var model = _mapper.Map<IEnumerable<Country>, IEnumerable<CountryDTO>>(await _database.Countries.GetAll());
            var existInDb = model.Any(it => it.Name == dto.Name);

            var selectAll = model.Where(it => it.Name == dto.Name);
            var edit = selectAll.Any(it => it.CountryID == dto.CountryID);

            if (edit == true)
            {
                existInDb = false;
            }

            return existInDb;
        }
    }
}
