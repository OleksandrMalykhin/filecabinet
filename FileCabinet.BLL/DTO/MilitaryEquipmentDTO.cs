﻿
namespace FileCabinet.BLL.DTO
{
    public class MilitaryEquipmentDTO
    {
        public int MilitaryEquipmentID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public double Rating { get; set; }

        public string ConvertedImage { get; set; }

        public int CategoryMilitaryEquipmentID { get; set; }
        public CategoryMilitaryEquipmentDTO CategoryMilitaryEquipment { get; set; }

        public int CountryID { get; set; }
        public CountryDTO Country { get; set; }

        public int TypeArmyID { get; set; }
        public TypeArmyDTO TypeArmy { get; set; }
    }
}
