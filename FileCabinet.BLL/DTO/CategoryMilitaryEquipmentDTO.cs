﻿using System.Collections.Generic;

namespace FileCabinet.BLL.DTO
{
    public class CategoryMilitaryEquipmentDTO
    {
        public int CategoryMilitaryEquipmentID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipmentDTO> MilitaryEquipment { get; set; }
    }
}
