﻿namespace FileCabinet.BLL.DTO
{
    public class ReviewsDTO
    {
        //public int ReviewsID { get; set; }
        public int Score { get; set; }


        public string UserProfileId { get; set; }
        public UserDTO User { get; set; }

        public int MilitaryEquipmentID { get; set; }
        public MilitaryEquipmentDTO MilitaryEquipments { get; set; }
    }
}
