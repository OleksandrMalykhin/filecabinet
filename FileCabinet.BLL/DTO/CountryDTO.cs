﻿using System.Collections.Generic;

namespace FileCabinet.BLL.DTO
{
    public class CountryDTO
    {
        public int CountryID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipmentDTO> MilitaryEquipment { get; set; }
    }
}
