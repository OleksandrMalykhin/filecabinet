﻿using System.Collections.Generic;

namespace FileCabinet.BLL.DTO
{
    public class TypeArmyDTO
    {
        public int TypeArmyID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipmentDTO> MilitaryEquipment { get; set; }
    }
}
