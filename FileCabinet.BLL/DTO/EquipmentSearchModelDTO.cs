﻿namespace FileCabinet.BLL.DTO
{
    public class EquipmentSearchModelDTO
    {
        public string SearchString { get; set; }
        public int? CategoryMilitaryEquipmentID { get; set; }
        public int? CountryID { get; set; }
        public int? TypeArmyID { get; set; }
    }
}
