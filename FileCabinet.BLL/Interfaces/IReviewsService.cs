﻿using FileCabinet.BLL.DTO;

namespace FileCabinet.BLL.Interfaces
{
    public interface IReviewsService
    {
        void AddRating(ReviewsDTO dto);
        ReviewsDTO FindReview(string userid, int? militaryeqipm);
        void EditReview(ReviewsDTO dto);

        void Dispose();
    }
}
