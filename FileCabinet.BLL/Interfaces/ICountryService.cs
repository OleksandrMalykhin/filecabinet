﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;

namespace FileCabinet.BLL.Interfaces
{
    public interface ICountryService
    {
        Task<IEnumerable<CountryDTO>> GetAll();
        CountryDTO Details(int? id);
        void Add(CountryDTO dto);
        Task Delete(CountryDTO dto);
        CountryDTO ConfimDelete(int? id);
        void Update(CountryDTO dto);
        Task<IEnumerable<CountryDTO>> GetAllForSelectList();
        Task<bool> CheckIfExistsCountry(CountryDTO dto);

        void Dispose();
    }
}
