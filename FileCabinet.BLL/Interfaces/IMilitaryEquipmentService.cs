﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;

namespace FileCabinet.BLL.Interfaces
{
    public interface IMilitaryEquipmentService
    {
        Task<IEnumerable<MilitaryEquipmentDTO>> GetAllEquipment();
        Task<IEnumerable<CountryDTO>> SelectTypeArmy(int? id);
        Task<IEnumerable<CategoryMilitaryEquipmentDTO>> SelectCountryGetCategory(int CountryID, int TypeArmyID);
        Task<IEnumerable<MilitaryEquipmentDTO>> SelectCategoryGetMilitaryEquipment(int? CategoryID, int? TypeArmyID, int? CountryID);
        MilitaryEquipmentDTO Details(int? id);
        IEnumerable<MilitaryEquipmentDTO> GetSortedEquipment(EquipmentSearchModelDTO searchModel);
        Task<bool> CheckIfExistsMilitaryEquipment(MilitaryEquipmentDTO dto);

        void Add(MilitaryEquipmentDTO dto);
        Task Delete(MilitaryEquipmentDTO dto);
        MilitaryEquipmentDTO ConfimDelete(int? id);
        void Update(MilitaryEquipmentDTO dto);
        void Dispose();
    }
}
