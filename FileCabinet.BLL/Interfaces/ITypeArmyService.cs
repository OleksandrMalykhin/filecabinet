﻿using FileCabinet.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileCabinet.BLL.Interfaces
{
    public interface ITypeArmyService
    {
        Task<IEnumerable<TypeArmyDTO>> GetAll();
        Task<IEnumerable<TypeArmyDTO>> GetAllForSelectList();
        Task<bool> CheckIfExistsTypeArmy(TypeArmyDTO dto);
        void Add(TypeArmyDTO dto);
        Task Delete(TypeArmyDTO dto);
        TypeArmyDTO ConfimDelete(int? id);
        void Update(TypeArmyDTO dto);

        void Dispose();
    }
}
