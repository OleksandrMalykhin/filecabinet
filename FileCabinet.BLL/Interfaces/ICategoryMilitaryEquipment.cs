﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileCabinet.BLL.DTO;

namespace FileCabinet.BLL.Interfaces
{
    public interface ICategoryMilitaryEquipment
    {
        Task<IEnumerable<CategoryMilitaryEquipmentDTO>> GetAll();
        void Add(CategoryMilitaryEquipmentDTO dto);
        Task Delete(CategoryMilitaryEquipmentDTO dto);
        CategoryMilitaryEquipmentDTO ConfimDelete(int? id);
        CategoryMilitaryEquipmentDTO Details(int? id);
        void Update(CategoryMilitaryEquipmentDTO dto);
        Task<IEnumerable<CategoryMilitaryEquipmentDTO>> GetAllForSelectList();

        Task<bool> CheckIfExistsCategory(CategoryMilitaryEquipmentDTO dto);

        void Dispose();
    }
}
