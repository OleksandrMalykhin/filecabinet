﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FileCabinet.Models
{
    public class CategoryMilitaryEquipmentViewModel
    {
        public int CategoryMilitaryEquipmentID { get; set; }

        [Required(ErrorMessage = "Enter category military equipment name")]
        [StringLength(15, MinimumLength = 3)]
        [Display(Name = "category name")]
        [Remote("CheckIfExistsCategory", "CategoryMilitaryEquipment", AdditionalFields = "CategoryMilitaryEquipmentID", ErrorMessage = "The category you entered already exists. Please specify a different category.")]
        public string Name { get; set; }

        [Display(Name = "Image")]
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipmentViewModel> MilitaryEquipment { get; set; }
    }
}