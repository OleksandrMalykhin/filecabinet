﻿using System.ComponentModel.DataAnnotations;

namespace FileCabinet.Models
{
    public class ReviewsViewModel
    {
        [Required(ErrorMessage = "Enter your rating")]
        [Range(0,5)]
        [Display(Name = "Rating")]
        public int Score { get; set; }

        public string UserProfileId { get; set; }
        public UserViewModel User { get; set; }

        public int MilitaryEquipmentID { get; set; }
        public MilitaryEquipmentViewModel MilitaryEquipments { get; set; }
    }
}