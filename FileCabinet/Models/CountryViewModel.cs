﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FileCabinet.Models
{
    public class CountryViewModel
    {
        public int CountryID { get; set; }

        [Required(ErrorMessage = "Enter country name")]
        [StringLength(40, MinimumLength = 3)]
        [Display(Name = "Country name")]
        [Remote("CheckIfExistsCountry", "Country", AdditionalFields = "CountryID", ErrorMessage = "The country name you entered already exists. Please enter a different country name.")]
        public string Name { get; set; }

        [Display(Name = "Image")]
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipmentViewModel> MilitaryEquipment { get; set; }
    }
}