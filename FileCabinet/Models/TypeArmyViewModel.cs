﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FileCabinet.Models
{
    public class TypeArmyViewModel
    {
        public int TypeArmyID { get; set; }

        [Required(ErrorMessage = "Enter type army name")]
        [StringLength(15, MinimumLength = 3)]
        [Display(Name = "Type army name")]
        [Remote("CheckIfExistsTypeArmy", "TypeArmy", AdditionalFields = "TypeArmyID", ErrorMessage = "The type of army you entered already exists. Please specify a different type of army.")]
        public string Name { get; set; }

        [Display(Name = "Image")]
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipmentViewModel> MilitaryEquipment { get; set; }
    }
}