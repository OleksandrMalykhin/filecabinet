﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FileCabinet.Models
{
    public class MilitaryEquipmentViewModel
    {
        public int MilitaryEquipmentID { get; set; }

        [Required(ErrorMessage = "Enter military equipment name")]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Equipment name")]
        [Remote("CheckIfExistsMilitaryEquipment", "MilitaryEquipment", AdditionalFields = "MilitaryEquipmentID", ErrorMessage = "The military equipment you entered already exists. Please specify a different military equipment name.")]
        public string Name { get; set; }

        [Display(Name = "Rating")]
        public decimal Rating { get; set; }

        [Display(Name = "Image")]
        public byte[] Image { get; set; }


        [Required(ErrorMessage = "Select military category")]
        [Display(Name = "Military category")]
        public int CategoryMilitaryEquipmentID { get; set; }
        public CategoryMilitaryEquipmentViewModel CategoryMilitaryEquipment { get; set; }

        [Required(ErrorMessage = "Select country")]
        [Display(Name = "Country")]
        public int CountryID { get; set; }
        public CountryViewModel Country { get; set; }

        [Required(ErrorMessage = "Select type army")]
        [Display(Name = "Type army")]
        public int TypeArmyID { get; set; }
        public TypeArmyViewModel TypeArmy { get; set; }
    }
}