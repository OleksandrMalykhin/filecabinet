﻿namespace FileCabinet.Models
{
    public class EquipmentReviewViewModel
    {
        public int MilitaryEquipmentID { get; set; }

        public string Name { get; set; }
        public decimal Rating { get; set; }
        public byte[] Image { get; set; }


        public int CategoryMilitaryEquipmentID { get; set; }
        public CategoryMilitaryEquipmentViewModel CategoryMilitaryEquipment { get; set; }

        public int CountryID { get; set; }
        public CountryViewModel Country { get; set; }

        public int TypeArmyID { get; set; }
        public TypeArmyViewModel TypeArmy { get; set; }


        public int Score { get; set; }

        public string UserProfileId { get; set; }
        public UserViewModel User { get; set; }
    }
}