﻿using Ninject.Modules;
using FileCabinet.BLL.Interfaces;
using FileCabinet.BLL.Services;

namespace FileCabinet.Util
{
    public class NinjectServices : NinjectModule
    {
        public override void Load()
        {
            Bind<IMilitaryEquipmentService>().To<MilitaryEquipmentService>();
            Bind<ITypeArmyService>().To<TypeArmyService>();
            Bind<ICountryService>().To<CountryService>();
            Bind<ICategoryMilitaryEquipment>().To<CategoryMilitaryEquipmentService>();
            Bind<IReviewsService>().To<ReviewsService>();
        }
    }
}