﻿using FileCabinet.BLL.Interfaces;
using System.Web.Mvc;
using FileCabinet.BLL.DTO;
using AutoMapper;
using FileCabinet.Models;
using System.Net;
using Microsoft.AspNet.Identity;

namespace FileCabinet.Controllers
{
    public class ReviewController : Controller
    {
        private readonly IReviewsService _reviewsService;
        private readonly IMapper _mapper;

        public ReviewController(IReviewsService serv)
        {
            _reviewsService = serv;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ReviewsDTO, ReviewsViewModel>().ReverseMap();
                cfg.CreateMap<UserViewModel, UserDTO>().ReverseMap();
                cfg.CreateMap<MilitaryEquipmentDTO, MilitaryEquipmentViewModel>().ReverseMap();

            }).CreateMapper();
        }

        [Authorize]
        public ActionResult AddRating(int? id, int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (id == null || TypeArmyID == null || CountryID == null || CategoryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.id = id;
                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;
                ViewBag.CategoryID = CategoryID;
                return View();
            }
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddRating([Bind(Include = "ReviewsID,Score,Id,MilitaryEquipmentID")] ReviewsViewModel review,
            int id, int CategoryID, int TypeArmyID, int CountryID)
        {
            review.MilitaryEquipmentID = id;
            review.UserProfileId = User.Identity.GetUserId();

            ViewBag.id = id;
            ViewBag.SelectedTypeArmyID = TypeArmyID;
            ViewBag.CountryID = CountryID;
            ViewBag.CategoryID = CategoryID;

            ViewBag.TypeArmyID = TypeArmyID;

            if (ModelState.IsValid)
            {
                try 
                {
                    var result = _mapper.Map<ReviewsViewModel, ReviewsDTO>(review);
                    _reviewsService.AddRating(result);
                    return RedirectToAction("SelectMilitaryEquipment", "MilitaryEquipment", new { ViewBag.id, ViewBag.CategoryID, ViewBag.TypeArmyID, ViewBag.CountryID });
                }
                catch
                {
                    ViewBag.Error = "Вы уже оставляли коментарий";
                    return RedirectToAction("SelectMilitaryEquipment", "MilitaryEquipment", new { ViewBag.id, ViewBag.CategoryID, ViewBag.TypeArmyID, ViewBag.CountryID });
                }
            }

            return View(review);
        }


        [Authorize]
        public ActionResult EditReview(int? id, int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (id == null || TypeArmyID == null || CountryID == null || CategoryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.id = id;
                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;
                ViewBag.CategoryID = CategoryID;

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var typeArmy = _mapper.Map<ReviewsDTO, ReviewsViewModel>(_reviewsService.FindReview(User.Identity.GetUserId(), id));
                if (typeArmy == null)
                {
                    return HttpNotFound();
                }
                return View(typeArmy);
            }
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditReview([Bind(Include = "ReviewsID,Score,Id,MilitaryEquipmentID")] ReviewsViewModel reviewmodel, int id, int CategoryID, int TypeArmyID, int CountryID)
        {
            reviewmodel.MilitaryEquipmentID = id;
            reviewmodel.UserProfileId = User.Identity.GetUserId();

            ViewBag.id = id;
            ViewBag.SelectedTypeArmyID = TypeArmyID;
            ViewBag.CountryID = CountryID;
            ViewBag.CategoryID = CategoryID;
            if (ModelState.IsValid)
            {
                var mapped = _mapper.Map<ReviewsViewModel, ReviewsDTO>(reviewmodel);
                _reviewsService.EditReview(mapped);
                return RedirectToAction("SelectMilitaryEquipment", "MilitaryEquipment", new { id, CategoryID, TypeArmyID, CountryID });
            }
            return View(reviewmodel);
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}