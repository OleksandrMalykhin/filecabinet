﻿using FileCabinet.BLL.Interfaces;
using System;
using System.Web;
using System.Web.Mvc;
using FileCabinet.BLL.DTO;
using AutoMapper;
using FileCabinet.Models;
using System.Net;
using System.Threading.Tasks;

namespace FileCabinet.Controllers
{
    public class CategoryMilitaryEquipmentController : Controller
    {

        private readonly ICategoryMilitaryEquipment _categoryMilitaryEquipment;

        private readonly IMapper _mapper;
        public CategoryMilitaryEquipmentController(ICategoryMilitaryEquipment service)
        {
            _categoryMilitaryEquipment = service;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmyViewModel, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Create(int? TypeArmyID, int? CountryID)
        {
            ViewBag.SelectedTypeArmyID = TypeArmyID;
            ViewBag.CountryID = CountryID;
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryMilitaryEquipmentID,Name")] CategoryMilitaryEquipmentViewModel country, HttpPostedFileBase image, int? TypeArmyID, int? CountryID)
        {
            if (TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;

                if (String.IsNullOrEmpty(country.Name))
                {
                    ModelState.AddModelError("Name", "Please enter type army name");
                }
                if (image == null)
                {
                    ModelState.AddModelError("Image", "Please add image");
                }

                if (ModelState.IsValid)
                {
                    if (image != null)
                    {
                        country.Image = new byte[image.ContentLength];
                        image.InputStream.Read(country.Image, 0, image.ContentLength);
                    }

                    var result = _mapper.Map<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>(country);
                    _categoryMilitaryEquipment.Add(result);
                    return RedirectToAction("SelectCountryGetCategory", "MilitaryEquipment", new { CountryID, TypeArmyID });
                }
                else
                {
                    return View(country);
                }
            }
        }

        public async Task<JsonResult> CheckIfExistsCategory([Bind(Include = "CategoryMilitaryEquipmentID,Name")] CategoryMilitaryEquipmentViewModel category)
        {
            var check = _mapper.Map<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>(category);

            var result = await _categoryMilitaryEquipment.CheckIfExistsCategory(check);

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id, int? TypeArmyID, int? CountryID)
        {
            if (id == null || TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.SelectedTypeArmyID = TypeArmyID;
            ViewBag.CountryID = CountryID;

            var typeArmy = _mapper.Map<CategoryMilitaryEquipmentDTO, CategoryMilitaryEquipmentViewModel>(_categoryMilitaryEquipment.ConfimDelete(id));
            if (typeArmy == null)
            {
                return HttpNotFound();
            }
            return View(typeArmy);
        }

        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int TypeArmyID, int CountryID)
        {
            ViewBag.SelectedTypeArmyID = TypeArmyID;
            ViewBag.CountryID = CountryID;

            CategoryMilitaryEquipmentDTO country = _categoryMilitaryEquipment.ConfimDelete(id);
            await _categoryMilitaryEquipment.Delete(country);

            return RedirectToAction("SelectCountryGetCategory", "MilitaryEquipment", new { CountryID = ViewBag.CountryID, TypeArmyID = ViewBag.SelectedTypeArmyID });
        }

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id, int? TypeArmyID, int? CountryID)
        {
            if (TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;
                var country = _mapper.Map<CategoryMilitaryEquipmentDTO, CategoryMilitaryEquipmentViewModel>(_categoryMilitaryEquipment.Details(id));
                if (country == null)
                {
                    return HttpNotFound();
                }
                return View(country);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryMilitaryEquipmentID,Name,Image")] CategoryMilitaryEquipmentViewModel category, int TypeArmyID, int CountryID, HttpPostedFileBase image)
        {
            ViewBag.SelectedTypeArmyID = TypeArmyID;
            ViewBag.CountryID = CountryID;
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    category.Image = new byte[image.ContentLength];
                    image.InputStream.Read(category.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>(category);
                _categoryMilitaryEquipment.Update(mapped);
                return RedirectToAction("SelectCountryGetCategory", "MilitaryEquipment", new { CountryID = ViewBag.CountryID, TypeArmyID = ViewBag.SelectedTypeArmyID });
            }
            return View(category);
        }
    }
}