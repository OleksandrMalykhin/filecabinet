﻿using AutoMapper;
using FileCabinet.BLL.DTO;
using FileCabinet.BLL.Interfaces;
using FileCabinet.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FileCabinet.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly IMilitaryEquipmentService _militaryEquipmentService;
        private readonly ICategoryMilitaryEquipment _categoryMilitaryEquipment;
        private readonly ITypeArmyService _typeArmyService;
        private readonly IReviewsService _reviewsService;

        private readonly IMapper _mapper;
        public AdminController(ICountryService serv, IMilitaryEquipmentService serv1,
            ICategoryMilitaryEquipment serv2, ITypeArmyService serv3, IReviewsService serv4)
        {
            _countryService = serv;
            _militaryEquipmentService = serv1;
            _categoryMilitaryEquipment = serv2;
            _typeArmyService = serv3;
            _reviewsService = serv4;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmyViewModel, TypeArmyDTO>().ReverseMap();
                cfg.CreateMap<ReviewsDTO, ReviewsViewModel>().ReverseMap();

                cfg.CreateMap<MilitaryEquipmentViewModel, EquipmentReviewViewModel>().ReverseMap();
                cfg.CreateMap<ReviewsViewModel, EquipmentReviewViewModel>().ReverseMap();
                cfg.CreateMap<EquipmentSearchModel, EquipmentSearchModelDTO>().ReverseMap();

            }).CreateMapper();
        }

        public async Task<ActionResult> GetAllTypeArmy()
        {
            var result = _mapper.Map<IEnumerable<TypeArmyDTO>, IEnumerable<TypeArmyViewModel>>(await _typeArmyService.GetAll());
            return View(result);
        }

        #region Admin CRUD type army


        public ActionResult CreateTypeArmy()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTypeArmy([Bind(Include = "TypeArmyID,Name")] TypeArmyViewModel typeArmy, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(typeArmy.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    typeArmy.Image = new byte[image.ContentLength];
                    image.InputStream.Read(typeArmy.Image, 0, image.ContentLength);
                }
                var result = _mapper.Map<TypeArmyViewModel, TypeArmyDTO>(typeArmy);
                _typeArmyService.Add(result);
                return RedirectToAction("GetAllTypeArmy");
            }
            else
            {
                return View(typeArmy);
            }
        }

        public ActionResult EditTypeArmy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeArmy = _mapper.Map<TypeArmyDTO, TypeArmyViewModel>(_typeArmyService.ConfimDelete(id));
            if (typeArmy == null)
            {
                return HttpNotFound();
            }
            return View(typeArmy);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTypeArmy([Bind(Include = "TypeArmyID,Name,Image")] TypeArmyViewModel typeArmy, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(typeArmy.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    typeArmy.Image = new byte[image.ContentLength];
                    image.InputStream.Read(typeArmy.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<TypeArmyViewModel, TypeArmyDTO>(typeArmy);
                _typeArmyService.Update(mapped);
                return RedirectToAction("GetAllTypeArmy");
            }
            else
            {
                return View(typeArmy);
            }
        }

        public ActionResult DeleteTypeArmy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeArmy = _mapper.Map<TypeArmyDTO, TypeArmyViewModel>(_typeArmyService.ConfimDelete(id));
            if (typeArmy == null)
            {
                return HttpNotFound();
            }
            return View(typeArmy);
        }

        [HttpPost, ActionName("DeleteTypeArmy")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TypeArmyDTO typeArmy = _typeArmyService.ConfimDelete(id);
            await _typeArmyService.Delete(typeArmy);

            return RedirectToAction("GetAllTypeArmy");
        }


        #endregion

        #region Admin CRUD Country


        public async Task<ActionResult> GetAllCountries()
        {
            var countries = _mapper.Map<IEnumerable<CountryDTO>, IEnumerable<CountryViewModel>>(await _countryService.GetAll());
            return View(countries);
        }

        public ActionResult CreateCountry()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCountry([Bind(Include = "CountryID,Name")] CountryViewModel country, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(country.Name))
            {
                ModelState.AddModelError("Name", "Please enter country name");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    country.Image = new byte[image.ContentLength];
                    image.InputStream.Read(country.Image, 0, image.ContentLength);
                }
                var result = _mapper.Map<CountryViewModel, CountryDTO>(country);
                _countryService.Add(result);
                return RedirectToAction("GetAllCountries");
            }
            else
            {
                return View(country);
            }
        }

        public ActionResult EditCountry(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var country = _mapper.Map<CountryDTO, CountryViewModel>(_countryService.Details(id));
                if (country == null)
                {
                    return HttpNotFound();
                }
                return View(country);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCountry([Bind(Include = "CountryID,Name,Image")] CountryViewModel country, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {

                if (image != null)
                {
                    country.Image = new byte[image.ContentLength];
                    image.InputStream.Read(country.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<CountryViewModel, CountryDTO>(country);
                _countryService.Update(mapped);
                return RedirectToAction("GetAllCountries");
            }
            else
            {
                return View(country);
            }
        }

        public ActionResult DeleteCountry(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var typeArmy = _mapper.Map<CountryDTO, CountryViewModel>(_countryService.ConfimDelete(id));
                if (typeArmy == null)
                {
                    return HttpNotFound();
                }
                return View(typeArmy);
            }
        }

        [HttpPost, ActionName("DeleteCountry")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedCountry(int id)
        {
            CountryDTO country = _countryService.ConfimDelete(id);
            await _countryService.Delete(country);

            return RedirectToAction("GetAllCountries");
        }


        #endregion

        #region Admin CRUD Category


        public async Task<ActionResult> GetAllCategories()
        {
            var category = _mapper.Map<IEnumerable<CategoryMilitaryEquipmentDTO>, IEnumerable<CategoryMilitaryEquipmentViewModel>>(await _categoryMilitaryEquipment.GetAll());
            return View(category);
        }

        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory([Bind(Include = "CategoryMilitaryEquipmentID,Name")] CategoryMilitaryEquipmentViewModel country, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(country.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    country.Image = new byte[image.ContentLength];
                    image.InputStream.Read(country.Image, 0, image.ContentLength);
                }

                var result = _mapper.Map<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>(country);
                _categoryMilitaryEquipment.Add(result);
                return RedirectToAction("GetAllCategories");
            }
            else
            {
                return View(country);
            }
        }

        public ActionResult EditCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var country = _mapper.Map<CategoryMilitaryEquipmentDTO, CategoryMilitaryEquipmentViewModel>(_categoryMilitaryEquipment.Details(id));
                if (country == null)
                {
                    return HttpNotFound();
                }
                return View(country);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory([Bind(Include = "CategoryMilitaryEquipmentID,Name,Image")] CategoryMilitaryEquipmentViewModel category, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    category.Image = new byte[image.ContentLength];
                    image.InputStream.Read(category.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>(category);
                _categoryMilitaryEquipment.Update(mapped);
                return RedirectToAction("GetAllCategories");
            }
            return View(category);
        }

        public ActionResult DeleteCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeArmy = _mapper.Map<CategoryMilitaryEquipmentDTO, CategoryMilitaryEquipmentViewModel>(_categoryMilitaryEquipment.ConfimDelete(id));
            if (typeArmy == null)
            {
                return HttpNotFound();
            }
            return View(typeArmy);
        }

        [HttpPost, ActionName("DeleteCategory")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedCategory(int id)
        {
            CategoryMilitaryEquipmentDTO category = _categoryMilitaryEquipment.ConfimDelete(id);
            await _categoryMilitaryEquipment.Delete(category);

            return RedirectToAction("GetAllCategories");
        }


        #endregion

        #region Admin CRUD MilitaryEquipment


        public async Task<ActionResult> GetAllMilitaryEquipment(EquipmentSearchModel searchModel)
        {
            var list_country = await _countryService.GetAllForSelectList();
            var list_category = await _categoryMilitaryEquipment.GetAllForSelectList();
            var list_typeArmy = await _typeArmyService.GetAllForSelectList();

            ViewBag.CategoryMilitaryEquipmentID = new SelectList(list_category, "CategoryMilitaryEquipmentID",
                "Name", searchModel.CategoryMilitaryEquipmentID);

            ViewBag.CountryID = new SelectList(list_country, "CountryID", "Name", searchModel.CountryID);
            ViewBag.TypeArmyID = new SelectList(list_typeArmy, "TypeArmyID", "Name", searchModel.TypeArmyID);

            var searchModeldto = _mapper.Map<EquipmentSearchModel, EquipmentSearchModelDTO>(searchModel);

            var result = _mapper.Map<IEnumerable<MilitaryEquipmentDTO>, IEnumerable<MilitaryEquipmentViewModel>>
                (_militaryEquipmentService.GetSortedEquipment(searchModeldto));

            return View(result);
        }

        public async Task<ActionResult> CreateMilitaryEquipment()
        {
            var list_country = await _countryService.GetAllForSelectList();
            var list_category = await _categoryMilitaryEquipment.GetAllForSelectList();
            var list_typeArmy = await _typeArmyService.GetAllForSelectList();

            ViewBag.CategoryMilitaryEquipmentID = new SelectList(list_category, "CategoryMilitaryEquipmentID", "Name");
            ViewBag.CountryID = new SelectList(list_country, "CountryID", "Name");
            ViewBag.TypeArmyID = new SelectList(list_typeArmy, "TypeArmyID", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMilitaryEquipment([Bind(Include = "MilitaryEquipmentID,Name,CategoryMilitaryEquipmentID,CountryID,TypeArmyID")]
            MilitaryEquipmentViewModel militaryEquipment, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(militaryEquipment.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }
            if (militaryEquipment.CountryID == 0)
            {
                ModelState.AddModelError("CountryID", "Please select country");
            }
            if (militaryEquipment.TypeArmyID == 0)
            {
                ModelState.AddModelError("TypeArmyID", "Please select type army");
            }
            if (militaryEquipment.CategoryMilitaryEquipmentID == 0)
            {
                ModelState.AddModelError("CategoryMilitaryEquipmentID", "Please military category");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }

            if (image.ContentType.ToLower() != ".jpg" &&
                    image.ContentType.ToLower() != ".jpeg" &&
                    image.ContentType.ToLower() != ".pjpeg" &&
                    image.ContentType.ToLower() != ".x-png" &&
                    image.ContentType.ToLower() != ".png")
            {
                ModelState.AddModelError("Image", "Not corrent format");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    militaryEquipment.Image = new byte[image.ContentLength];
                    image.InputStream.Read(militaryEquipment.Image, 0, image.ContentLength);
                }

                var result = _mapper.Map<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>(militaryEquipment);
                _militaryEquipmentService.Add(result);
                return RedirectToAction("GetAllMilitaryEquipment");
            }

            return View(militaryEquipment);
        }

        public async Task<ActionResult> EditMilitaryEquipment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var data = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipmentViewModel>(_militaryEquipmentService.Details(id));
                if (data == null)
                {
                    return HttpNotFound();
                }

                var list_country = await _countryService.GetAllForSelectList();
                var list_category = await _categoryMilitaryEquipment.GetAllForSelectList();
                var list_typeArmy = await _typeArmyService.GetAllForSelectList();

                ViewBag.CategoryMilitaryEquipmentID = new SelectList(list_category, "CategoryMilitaryEquipmentID", "Name", data.CategoryMilitaryEquipmentID);
                ViewBag.CountryID = new SelectList(list_country, "CountryID", "Name", data.CountryID);
                ViewBag.TypeArmyID = new SelectList(list_typeArmy, "TypeArmyID", "Name", data.TypeArmyID);

                return View(data);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMilitaryEquipment([Bind(Include = "MilitaryEquipmentID,Name,CategoryMilitaryEquipmentID,CountryID,TypeArmyID,Image")]
            MilitaryEquipmentViewModel militaryEquipment, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    militaryEquipment.Image = new byte[image.ContentLength];
                    image.InputStream.Read(militaryEquipment.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>(militaryEquipment);
                _militaryEquipmentService.Update(mapped);
                return RedirectToAction("GetAllMilitaryEquipment");
            }

            return View(militaryEquipment);
        }


        public ActionResult DeleteMilitaryEquipment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var typeArmy = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipmentViewModel>(_militaryEquipmentService.ConfimDelete(id));
                if (typeArmy == null)
                {
                    return HttpNotFound();
                }
                return View(typeArmy);
            }
        }

        [HttpPost, ActionName("DeleteMilitaryEquipment")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedMilitaryEquipment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                MilitaryEquipmentDTO country = _militaryEquipmentService.ConfimDelete(id);
                await _militaryEquipmentService.Delete(country);

                return RedirectToAction("GetAllMilitaryEquipment");
            }
        }

        #endregion

        #region Check if exists in DataBase


        public async Task<JsonResult> CheckIfExistsTypeArmy([Bind(Include = "TypeArmyID,Name")] TypeArmyViewModel typeArmy)
        {
            var check = _mapper.Map<TypeArmyViewModel, TypeArmyDTO>(typeArmy);

            var result = (await _typeArmyService.CheckIfExistsTypeArmy(check));

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> CheckIfExistsCountry([Bind(Include = "CountryID,Name")] CountryViewModel country)
        {
            var check = _mapper.Map<CountryViewModel, CountryDTO>(country);

            var result = await _countryService.CheckIfExistsCountry(check);

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> CheckIfExistsCategory([Bind(Include = "CategoryMilitaryEquipmentID,Name")] CategoryMilitaryEquipmentViewModel category)
        {
            var check = _mapper.Map<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>(category);

            var result = await _categoryMilitaryEquipment.CheckIfExistsCategory(check);

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> CheckIfExistsMilitaryEquipment([Bind(Include = "MilitaryEquipmentID,Name,CategoryMilitaryEquipmentID,CountryID,TypeArmyID")] MilitaryEquipmentViewModel viewmodel)
        {
            var check = _mapper.Map<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>(viewmodel);

            var result = await _militaryEquipmentService.CheckIfExistsMilitaryEquipment(check);

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        
        
        #endregion
    }
}