﻿using FileCabinet.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using FileCabinet.BLL.DTO;
using AutoMapper;
using FileCabinet.Models;
using System.Threading.Tasks;
using System.Net;

namespace FileCabinet.Controllers
{
    public class TypeArmyController : Controller
    {
        private readonly ITypeArmyService _typeArmyService;
        private readonly IMapper _mapper;

        public TypeArmyController(ITypeArmyService serv)
        {
            _typeArmyService = serv;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmyViewModel, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            var result = _mapper.Map<IEnumerable<TypeArmyDTO>, IEnumerable<TypeArmyViewModel>>(await _typeArmyService.GetAll());
            return View(result);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TypeArmyID,Name")] TypeArmyViewModel typeArmy, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(typeArmy.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }


            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    typeArmy.Image = new byte[image.ContentLength];
                    image.InputStream.Read(typeArmy.Image, 0, image.ContentLength);
                }
                var result = _mapper.Map<TypeArmyViewModel, TypeArmyDTO>(typeArmy);
                _typeArmyService.Add(result);
                return RedirectToAction("Index");
            }
            else
            {
                return View(typeArmy);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeArmy = _mapper.Map<TypeArmyDTO, TypeArmyViewModel>(_typeArmyService.ConfimDelete(id));
            if (typeArmy == null)
            {
                return HttpNotFound();
            }
            return View(typeArmy);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TypeArmyID,Name,Image")] TypeArmyViewModel typeArmy, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(typeArmy.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    typeArmy.Image = new byte[image.ContentLength];
                    image.InputStream.Read(typeArmy.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<TypeArmyViewModel, TypeArmyDTO>(typeArmy);
                _typeArmyService.Update(mapped);
                return RedirectToAction("Index");
            }
            else
            {
                return View(typeArmy);
            }
        }

        public async Task<JsonResult> CheckIfExistsTypeArmy([Bind(Include = "TypeArmyID,Name")] TypeArmyViewModel typeArmy)
        {
            var check = _mapper.Map<TypeArmyViewModel, TypeArmyDTO>(typeArmy);

            var result = (await _typeArmyService.CheckIfExistsTypeArmy(check));

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeArmy = _mapper.Map<TypeArmyDTO, TypeArmyViewModel>(_typeArmyService.ConfimDelete(id));
            if (typeArmy == null)
            {
                return HttpNotFound();
            }
            return View(typeArmy);
        }

        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TypeArmyDTO typeArmy = _typeArmyService.ConfimDelete(id);
            await _typeArmyService.Delete(typeArmy);

            return RedirectToAction("Index");
        }
    }
}