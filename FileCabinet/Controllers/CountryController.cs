﻿using FileCabinet.BLL.Interfaces;
using System;
using System.Web;
using System.Web.Mvc;
using FileCabinet.BLL.DTO;
using AutoMapper;
using FileCabinet.Models;
using System.Net;
using System.Threading.Tasks;

namespace FileCabinet.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountryService _countryService;

        private readonly IMapper _mapper;
        public CountryController(ICountryService serv)
        {
            _countryService = serv;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmyViewModel, TypeArmyDTO>().ReverseMap();

            }).CreateMapper();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Details(int? id, int? TypeArmyID)
        {
            if (id == null || TypeArmyID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.TypeArmyID = TypeArmyID;

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var reslut = _mapper.Map<CountryDTO, CountryViewModel>(_countryService.Details(id));
                if (reslut == null)
                {
                    return HttpNotFound();
                }
                return View(reslut);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Create(int TypeArmyID)
        {

            ViewBag.TypeArmyID = TypeArmyID;

            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CountryID,Name")] CountryViewModel country, int TypeArmyID, HttpPostedFileBase image)
        {

            ViewBag.TypeArmyID = TypeArmyID;

            if (String.IsNullOrEmpty(country.Name))
            {
                ModelState.AddModelError("Name", "Please enter country name");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    country.Image = new byte[image.ContentLength];
                    image.InputStream.Read(country.Image, 0, image.ContentLength);
                }
                var result = _mapper.Map<CountryViewModel, CountryDTO>(country);
                _countryService.Add(result);
                return RedirectToAction("Select", "MilitaryEquipment", new { id = TypeArmyID });
            }
            else
            {
                return View(country);
            }
        }

        public async Task<JsonResult> CheckIfExistsCountry([Bind(Include = "CountryID,Name")] CountryViewModel country)
        {
            var check = _mapper.Map<CountryViewModel, CountryDTO>(country);

            var result = await _countryService.CheckIfExistsCountry(check);

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id, int? TypeArmyID)
        {
            if (id == null || TypeArmyID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.TypeArmyID = TypeArmyID;

                var typeArmy = _mapper.Map<CountryDTO, CountryViewModel>(_countryService.ConfimDelete(id));
                if (typeArmy == null)
                {
                    return HttpNotFound();
                }
                return View(typeArmy);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int TypeArmyID)
        {
            ViewBag.TypeArmyID = TypeArmyID;

            CountryDTO country = _countryService.ConfimDelete(id);
            await _countryService.Delete(country);

            return RedirectToAction("Select", "MilitaryEquipment", new { id = ViewBag.CategoryID });
        }

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id, int? TypeArmyID)
        {
            if (id == null || TypeArmyID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {

                ViewBag.TypeArmyID = TypeArmyID;
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var country = _mapper.Map<CountryDTO, CountryViewModel>(_countryService.Details(id));
                if (country == null)
                {
                    return HttpNotFound();
                }
                return View(country);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CountryID,Name,Image")] CountryViewModel country, int TypeArmyID, HttpPostedFileBase image)
        {
            ViewBag.TypeArmyID = TypeArmyID;
            if (ModelState.IsValid)
            {

                if (image != null)
                {
                    country.Image = new byte[image.ContentLength];
                    image.InputStream.Read(country.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<CountryViewModel, CountryDTO>(country);
                _countryService.Update(mapped);
                return RedirectToAction("Select", "MilitaryEquipment", new { id = ViewBag.TypeArmyID });
            }
            else
            {
                return View(country);
            }
        }
    }
}