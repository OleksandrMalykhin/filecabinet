﻿using FileCabinet.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using FileCabinet.BLL.DTO;
using AutoMapper;
using FileCabinet.Models;
using System.Net;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace FileCabinet.Controllers
{
    public class MilitaryEquipmentController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly IMilitaryEquipmentService _militaryEquipmentService;
        private readonly ICategoryMilitaryEquipment _categoryMilitaryEquipment;
        private readonly ITypeArmyService _typeArmyService;
        private readonly IReviewsService _reviewsService;

        private readonly IMapper _mapper;
        public MilitaryEquipmentController(ICountryService serv, IMilitaryEquipmentService serv1, 
            ICategoryMilitaryEquipment serv2, ITypeArmyService serv3, IReviewsService serv4)
        {
            _countryService = serv;
            _militaryEquipmentService = serv1;
            _categoryMilitaryEquipment = serv2;
            _typeArmyService = serv3;
            _reviewsService = serv4;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmyViewModel, TypeArmyDTO>().ReverseMap();
                cfg.CreateMap<ReviewsDTO, ReviewsViewModel>().ReverseMap();
                cfg.CreateMap<MilitaryEquipmentViewModel, EquipmentReviewViewModel>().ReverseMap();
                cfg.CreateMap<ReviewsViewModel, EquipmentReviewViewModel>().ReverseMap();

            }).CreateMapper();
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Create(int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (TypeArmyID == null || CountryID == null || CategoryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Nav_SelectedTypeArmyID = TypeArmyID;
            ViewBag.Nav_CountryID = CountryID;
            ViewBag.Nav_CategoryID = CategoryID;

            var list_country = await _countryService.GetAllForSelectList();
            var list_category = await _categoryMilitaryEquipment.GetAllForSelectList();
            var list_typeArmy = await _typeArmyService.GetAllForSelectList();

            ViewBag.CategoryMilitaryEquipmentID = new SelectList(list_category, "CategoryMilitaryEquipmentID", "Name");
            ViewBag.CountryID = new SelectList(list_country, "CountryID", "Name");
            ViewBag.TypeArmyID = new SelectList(list_typeArmy, "TypeArmyID", "Name");

            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MilitaryEquipmentID,Name,CategoryMilitaryEquipmentID,CountryID,TypeArmyID")]
            MilitaryEquipmentViewModel militaryEquipment, int? CategoryID, int? TypeArmyID, int? CountryID, HttpPostedFileBase image)
        {
            if (String.IsNullOrEmpty(militaryEquipment.Name))
            {
                ModelState.AddModelError("Name", "Please enter type army name");
            }
            if (militaryEquipment.CountryID == 0)
            {
                ModelState.AddModelError("CountryID", "Please select country");
            }
            if (militaryEquipment.TypeArmyID == 0)
            {
                ModelState.AddModelError("TypeArmyID", "Please select type army");
            }
            if (militaryEquipment.CategoryMilitaryEquipmentID == 0)
            {
                ModelState.AddModelError("CategoryMilitaryEquipmentID", "Please military category");
            }
            if (image == null)
            {
                ModelState.AddModelError("Image", "Please add image");
            }

            if (image.ContentType.ToLower() != ".jpg" &&
        image.ContentType.ToLower() != ".jpeg" &&
        image.ContentType.ToLower() != ".pjpeg" &&
        image.ContentType.ToLower() != ".x-png" &&
        image.ContentType.ToLower() != ".png")
            {
                ModelState.AddModelError("Image", "Not corrent format");
            }

            ViewBag.Nav_SelectedTypeArmyID = TypeArmyID;
            ViewBag.Nav_CountryID = CountryID;
            ViewBag.Nav_CategoryID = CategoryID;

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    militaryEquipment.Image = new byte[image.ContentLength];
                    image.InputStream.Read(militaryEquipment.Image, 0, image.ContentLength);
                }

                var result = _mapper.Map<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>(militaryEquipment);
                _militaryEquipmentService.Add(result);
                return RedirectToAction("SelectCategoryGetMilitaryEquipment", "MilitaryEquipment", new { CategoryID = militaryEquipment.CategoryMilitaryEquipmentID, militaryEquipment.TypeArmyID, militaryEquipment.CountryID });
            }

            return View(militaryEquipment);
        }

        public async Task<JsonResult> CheckIfExistsMilitaryEquipment([Bind(Include = "MilitaryEquipmentID,Name,CategoryMilitaryEquipmentID,CountryID,TypeArmyID")] MilitaryEquipmentViewModel viewmodel)
        {
            var check = _mapper.Map<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>(viewmodel);

            var result = await _militaryEquipmentService.CheckIfExistsMilitaryEquipment(check);

            if (result == false)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(int? id, int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (id == null || TypeArmyID == null || CountryID == null || CategoryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.Nav_SelectedTypeArmyID = TypeArmyID;
                ViewBag.Nav_CountryID = CountryID;
                ViewBag.Nav_CategoryID = CategoryID;

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var data = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipmentViewModel>(_militaryEquipmentService.Details(id));
                if (data == null)
                {
                    return HttpNotFound();
                }

                var list_country = await _countryService.GetAllForSelectList();
                var list_category = await _categoryMilitaryEquipment.GetAllForSelectList();
                var list_typeArmy = await _typeArmyService.GetAllForSelectList();

                ViewBag.CategoryMilitaryEquipmentID = new SelectList(list_category, "CategoryMilitaryEquipmentID", "Name", data.CategoryMilitaryEquipmentID);
                ViewBag.CountryID = new SelectList(list_country, "CountryID", "Name", data.CountryID);
                ViewBag.TypeArmyID = new SelectList(list_typeArmy, "TypeArmyID", "Name", data.TypeArmyID);

                return View(data);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MilitaryEquipmentID,Name,CategoryMilitaryEquipmentID,CountryID,TypeArmyID,Image")]
            MilitaryEquipmentViewModel militaryEquipment, int? CategoryID, int? TypeArmyID, int? CountryID, HttpPostedFileBase image)
        {
            ViewBag.Nav_SelectedTypeArmyID = TypeArmyID;
            ViewBag.Nav_CountryID = CountryID;
            ViewBag.Nav_CategoryID = CategoryID;

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    militaryEquipment.Image = new byte[image.ContentLength];
                    image.InputStream.Read(militaryEquipment.Image, 0, image.ContentLength);
                }

                var mapped = _mapper.Map<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>(militaryEquipment);
                _militaryEquipmentService.Update(mapped);
                return RedirectToAction("SelectCategoryGetMilitaryEquipment", "MilitaryEquipment", new { CategoryID = militaryEquipment.CategoryMilitaryEquipmentID, militaryEquipment.TypeArmyID, militaryEquipment.CountryID });
            }
            return View(militaryEquipment);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id, int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if(id == null || CategoryID == null || TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.Nav_SelectedTypeArmyID = TypeArmyID;
                ViewBag.Nav_CountryID = CountryID;
                ViewBag.Nav_CategoryID = CategoryID;

                var typeArmy = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipmentViewModel>(_militaryEquipmentService.ConfimDelete(id));
                if (typeArmy == null)
                {
                    return HttpNotFound();
                }
                return View(typeArmy);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed (int? id, int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (id == null || CategoryID == null || TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.Nav_SelectedTypeArmyID = TypeArmyID;
                ViewBag.Nav_CountryID = CountryID;
                ViewBag.Nav_CategoryID = CategoryID;

                MilitaryEquipmentDTO country = _militaryEquipmentService.ConfimDelete(id);
                await _militaryEquipmentService.Delete(country);

                return RedirectToAction("SelectCategoryGetMilitaryEquipment", new { CategoryID = ViewBag.Nav_CategoryID, TypeArmyID = ViewBag.Nav_SelectedTypeArmyID, CountryID = ViewBag.Nav_CountryID });
            }
        }

        /// <summary>
        /// The method selects countries according to the selected category
        /// </summary>
        /// <param name="id"> selected type army </param>
        /// <returns>
        /// Returns the countries where the selected army type is
        /// </returns>
        [AllowAnonymous]
        public async Task<ActionResult> Select(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            else
            {
                var countries = _mapper.Map<IEnumerable<CountryDTO>, IEnumerable<CountryViewModel>>(await _militaryEquipmentService.SelectTypeArmy(id));

                ViewBag.TypeArmyID = id;
                return View(countries);
            }
        }

        /// <summary>
        /// Selects the categories that the country has
        /// </summary>
        /// <param name="CountryID"> selected country </param>
        /// <param name="TypeArmyID"> selected type army </param>
        /// <returns>
        /// Returns the categories that the selected country has
        /// </returns>
        [AllowAnonymous]
        public async Task<ActionResult> SelectCountryGetCategory(int? CountryID, int? TypeArmyID)
        {
            if(CountryID == null || TypeArmyID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            else
            {
                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;

                var category = _mapper.Map<IEnumerable<CategoryMilitaryEquipmentDTO>, IEnumerable<CategoryMilitaryEquipmentViewModel>>(await _militaryEquipmentService.SelectCountryGetCategory((int)CountryID, (int)TypeArmyID));

                return View(category);
            }
        }

        /// <summary>
        /// Selects user-selected military vehicles
        /// </summary>
        /// <param name="CategoryID"> selected category </param>
        /// <param name="TypeArmyID"> selected type army </param>
        /// <param name="CountryID"> selected country </param>
        /// <returns>
        /// Returns selected military equipment
        /// </returns>
        [AllowAnonymous]
        public async Task<ActionResult> SelectCategoryGetMilitaryEquipment(int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (CategoryID == null || TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;
                ViewBag.CategoryID = CategoryID;

                var militaryEquipment = _mapper.Map<IEnumerable<MilitaryEquipmentDTO>, IEnumerable<MilitaryEquipmentViewModel>>(await _militaryEquipmentService.SelectCategoryGetMilitaryEquipment(CategoryID, TypeArmyID, CountryID));

                return View(militaryEquipment);
            }
        }

        /// <summary>
        /// The method finds the selected military equipment and the user's rating
        /// </summary>
        /// <param name="id"> selected military equipment id</param>
        /// <param name="CategoryID"> selecetd category </param>
        /// <param name="TypeArmyID"> selected type army </param>
        /// <param name="CountryID"> selected country </param>
        /// <returns>
        /// Returns the selected military vehicle
        /// </returns>
        [AllowAnonymous]
        public ActionResult SelectMilitaryEquipment(int? id, int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            if (id == null || CategoryID == null || TypeArmyID == null || CountryID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var Equipment = _mapper.Map<MilitaryEquipmentDTO, MilitaryEquipmentViewModel>(_militaryEquipmentService.Details(id));
                if (Equipment == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }

                var review = _mapper.Map<ReviewsDTO, ReviewsViewModel>(_reviewsService.FindReview(User.Identity.GetUserId(), id));

                var result = _mapper.Map<MilitaryEquipmentViewModel, EquipmentReviewViewModel>(Equipment);
                var review1 = _mapper.Map<ReviewsViewModel, EquipmentReviewViewModel>(review);

                ViewBag.SelectedTypeArmyID = TypeArmyID;
                ViewBag.CountryID = CountryID;
                ViewBag.CategoryID = CategoryID;

                if (review1 == null)
                {
                    return View(result);
                }
                else
                {
                    result.Score = review1.Score;
                    return View(result);
                }
            }
        }
    }
}