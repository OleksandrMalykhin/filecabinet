﻿using FileCabinet.BLL.Interfaces;
using System.Collections.Generic;
using System.Web.Mvc;
using FileCabinet.BLL.DTO;
using AutoMapper;
using FileCabinet.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Linq;

namespace FileCabinet.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly IMilitaryEquipmentService _militaryEquipmentService;
        private readonly ICategoryMilitaryEquipment _categoryMilitaryEquipment;
        private readonly ITypeArmyService _typeArmyService;
        private readonly IReviewsService _reviewsService;

        private readonly IMapper _mapper;

        public HomeController(ICountryService serv, IMilitaryEquipmentService serv1,
            ICategoryMilitaryEquipment serv2, ITypeArmyService serv3, IReviewsService serv4)
        {
            _countryService = serv;
            _militaryEquipmentService = serv1;
            _categoryMilitaryEquipment = serv2;
            _typeArmyService = serv3;
            _reviewsService = serv4;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MilitaryEquipmentViewModel, MilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
                cfg.CreateMap<CategoryMilitaryEquipmentViewModel, CategoryMilitaryEquipmentDTO>().ReverseMap();
                cfg.CreateMap<TypeArmyViewModel, TypeArmyDTO>().ReverseMap();
                cfg.CreateMap<EquipmentSearchModel, EquipmentSearchModelDTO>().ReverseMap();

            }).CreateMapper();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.userid = User.Identity.GetUserId();
            ViewBag.Message = "Your application description page.";
            bool result = User.IsInRole("admin");

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<ActionResult> SearchView(EquipmentSearchModel searchModel)
        {
            var list_country = await _countryService.GetAllForSelectList();
            var list_category = await _categoryMilitaryEquipment.GetAllForSelectList();
            var list_typeArmy = await _typeArmyService.GetAllForSelectList();

            ViewBag.CategoryMilitaryEquipmentID = new SelectList(list_category, "CategoryMilitaryEquipmentID",
                "Name", searchModel.CategoryMilitaryEquipmentID);

            ViewBag.CountryID = new SelectList(list_country, "CountryID", "Name", searchModel.CountryID);
            ViewBag.TypeArmyID = new SelectList(list_typeArmy, "TypeArmyID", "Name", searchModel.TypeArmyID);

            var searchModeldto = _mapper.Map<EquipmentSearchModel, EquipmentSearchModelDTO>(searchModel);

            var result = _mapper.Map<IEnumerable<MilitaryEquipmentDTO>, IEnumerable<MilitaryEquipmentViewModel>>
                (_militaryEquipmentService.GetSortedEquipment(searchModeldto));

            return View(result);
        }
    }
}