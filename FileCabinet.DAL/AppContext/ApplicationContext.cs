﻿using System.Data.Entity;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FileCabinet.DAL.AppContext
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        static ApplicationContext()
        {
            Database.SetInitializer<ApplicationContext>(new DbSeed());
        }

        public ApplicationContext(string connectionString) : base(connectionString)
        {

        }

        public ApplicationContext() : base("DefaultConnection")
        {

        }

        public DbSet<UserProfile> ClientProfiles { get; set; }
        public DbSet<TypeArmy> TypeArmies { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CategoryMilitaryEquipment> CategoryMilitaryEquipments { get; set; }
        public DbSet<MilitaryEquipment> MilitaryEquipments { get; set; }
        public DbSet<Reviews> Reviews { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Reviews>()
                .HasKey(c => new { c.MilitaryEquipmentID, c.UserProfileId });
            base.OnModelCreating(modelBuilder);
        }

    }
}
