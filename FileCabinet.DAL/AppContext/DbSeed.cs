﻿using System.Data.Entity;
using FileCabinet.DAL.Entities;
using System.IO;

namespace FileCabinet.DAL.AppContext
{
    public class DbSeed : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext db)
        {

            //Виды армий
            db.TypeArmies.Add(new TypeArmy { TypeArmyID = 1, Name = "Ground",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/groundU.jpg")
            });
            db.TypeArmies.Add(new TypeArmy { TypeArmyID = 2, Name = "Air",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/air.jpg") });
            db.TypeArmies.Add(new TypeArmy { TypeArmyID = 3, Name = "Water",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/water.jpg")
            });

            //Страны
            db.Countries.Add(new Country { CountryID = 1, Name = "Russia",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/Russia.jpg")
            });
            db.Countries.Add(new Country { CountryID = 2, Name = "USA",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/USA.jpg")
            });
            db.Countries.Add(new Country { CountryID = 3, Name = "Ukraine",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/Ukraine.jpg")
            });


            //Категории в армии
            db.CategoryMilitaryEquipments.Add(new CategoryMilitaryEquipment { CategoryMilitaryEquipmentID = 1, Name = "MBT",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/MBT.jpg") });
            db.CategoryMilitaryEquipments.Add(new CategoryMilitaryEquipment { CategoryMilitaryEquipmentID = 2, Name = "Light tank",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/Light tank.jpg") });
            db.CategoryMilitaryEquipments.Add(new CategoryMilitaryEquipment { CategoryMilitaryEquipmentID = 3, Name = "Medium tank",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/Medium tank.jpg") });
            db.CategoryMilitaryEquipments.Add(new CategoryMilitaryEquipment { CategoryMilitaryEquipmentID = 4, Name = "Heavy tank",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/Heavy tank.jpg") });
            db.CategoryMilitaryEquipments.Add(new CategoryMilitaryEquipment { CategoryMilitaryEquipmentID = 5, Name = "Interceptor",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/MIG-25.jpg")
            });
            db.CategoryMilitaryEquipments.Add(new CategoryMilitaryEquipment { CategoryMilitaryEquipmentID = 6, Name = "Stormtrooper",
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/SU-25.jpg")
            });


            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 1, TypeArmyID = 1, CountryID = 1, Name = "T-90", CategoryMilitaryEquipmentID = 1,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/T-90.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 2, TypeArmyID = 1, CountryID = 1, Name = "T-72", CategoryMilitaryEquipmentID = 1,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/T-72.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 3, TypeArmyID = 2, CountryID = 1, Name = "SU-25", CategoryMilitaryEquipmentID = 6,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/SU-25.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 4, TypeArmyID = 2, CountryID = 1, Name = "SU-17", CategoryMilitaryEquipmentID = 6,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/SU-17.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment
            {
                MilitaryEquipmentID = 5,
                TypeArmyID = 2,
                CountryID = 1,
                Name = "MIG-25",
                CategoryMilitaryEquipmentID = 5,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/MIG-25.jpg")
            });

            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 6, TypeArmyID = 1, CountryID = 2, Name = "Abrams M1A1", CategoryMilitaryEquipmentID = 1,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/M1A1.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 7, TypeArmyID = 1, CountryID = 2, Name = "Abrams M1A2", CategoryMilitaryEquipmentID = 1,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/M1A2.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 8, TypeArmyID = 2, CountryID = 2, Name = "F15", CategoryMilitaryEquipmentID = 6,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/F15.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 9, TypeArmyID = 2, CountryID = 2, Name = "F22", CategoryMilitaryEquipmentID = 6,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/F22.jpg")
            });

            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 10, TypeArmyID = 1, CountryID = 3, Name = "T-80", CategoryMilitaryEquipmentID = 1,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/T-64BV.jpg")
            });
            db.MilitaryEquipments.Add(new MilitaryEquipment { MilitaryEquipmentID = 11, TypeArmyID = 1, CountryID = 3, Name = "T-64", CategoryMilitaryEquipmentID = 1,
                Image = ReadFile("C:/Users/atom5/source/repos/FileCabinet/FileCabinet.DAL/Image/T-80OP.jpg")
            });



            db.SaveChanges();
        }

        public static byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes 
            //to read from file.
            //In this case we want to read entire file. 
            //So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            return data;
        }
    }
}
