﻿using FileCabinet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.AppContext;
using System.Data.Entity;

namespace FileCabinet.DAL.Repositories
{
    class CountryRepositories : IRepository<Country>
    {
        private readonly ApplicationContext _db;

        public CountryRepositories(ApplicationContext context)
        {
            _db = context;
        }

        public void Create(Country data)
        {
            _db.Countries.Add(data);
            _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            Country data = await _db.Countries.FindAsync(id);
            if (data != null)
                _db.Countries.Remove(data);

            await _db.SaveChangesAsync();
        }

        public IEnumerable<Country> Find(Func<Country, bool> predicate)
        {
            return _db.Countries.Where(predicate).ToList();
        }

        public async Task<IEnumerable<Country>> GetAll()
        {
            return await _db.Countries.ToListAsync();
        }

        public async Task<Country> GetById(int id)
        {
            return await _db.Countries.FindAsync(id);
        }

        public void Update(Country data)
        {
            _db.Entry(data).State = EntityState.Modified;
            _db.SaveChangesAsync();
        }
    }
}
