﻿using FileCabinet.DAL.AppContext;
using FileCabinet.DAL.Interfaces;
using System;
using FileCabinet.DAL.Entities;

namespace FileCabinet.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _db;
        private CategoryMilitaryEquipmentRepository _categoryMilitaryEquipmentRepository;
        private CountryRepositories _countryRepositories;
        private MilitaryEquipmentRepository _militaryEquipmentRepository;
        private ReviewsRepository _reviewsRepository;
        private TypeArmyRepository _typeArmyRepository;

        private bool disposed = false;

        public EFUnitOfWork(string connectionString)
        {
            _db = new ApplicationContext(connectionString);
        }

        public IRepository<CategoryMilitaryEquipment> CategoryMilitaryEquipments
        {
            get
            {
                if (_categoryMilitaryEquipmentRepository == null)
                    _categoryMilitaryEquipmentRepository = new CategoryMilitaryEquipmentRepository(_db);
                return _categoryMilitaryEquipmentRepository;
            }
        }

        public IRepository<Country> Countries
        {
            get
            {
                if (_countryRepositories == null)
                    _countryRepositories = new CountryRepositories(_db);
                return _countryRepositories;
            }
        }

        public IMilitaryEquipmentRepository<MilitaryEquipment> MilitaryEquipments
        {
            get
            {
                if (_militaryEquipmentRepository == null)
                    _militaryEquipmentRepository = new MilitaryEquipmentRepository(_db);
                return _militaryEquipmentRepository;
            }
        }

        public IRepository<Reviews> Reviews
        {
            get
            {
                if (_reviewsRepository == null)
                    _reviewsRepository = new ReviewsRepository(_db);
                return _reviewsRepository;
            }
        }

        public IRepository<TypeArmy> TypeArmies
        {
            get
            {
                if (_typeArmyRepository == null)
                    _typeArmyRepository = new TypeArmyRepository(_db);
                return _typeArmyRepository;
            }
        }

        public void Save()
        {
            _db.SaveChangesAsync();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
