﻿using FileCabinet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.AppContext;
using System.Data.Entity;

namespace FileCabinet.DAL.Repositories
{
    public class CategoryMilitaryEquipmentRepository : IRepository<CategoryMilitaryEquipment>
    {
        private readonly ApplicationContext _db;

        public CategoryMilitaryEquipmentRepository(ApplicationContext context)
        {
            _db = context;
        }

        public void Create(CategoryMilitaryEquipment data)
        {
            _db.CategoryMilitaryEquipments.Add(data);
            _db.SaveChanges();
        }

        public async Task Delete(int id)
        {
            CategoryMilitaryEquipment news = await _db.CategoryMilitaryEquipments.FindAsync(id);
            if (news != null)
                _db.CategoryMilitaryEquipments.Remove(news);

            await _db.SaveChangesAsync();
        }

        public IEnumerable<CategoryMilitaryEquipment> Find(Func<CategoryMilitaryEquipment, bool> predicate)
        {
            return _db.CategoryMilitaryEquipments.Where(predicate).ToList();
        }

        public async Task<IEnumerable<CategoryMilitaryEquipment>> GetAll()
        {
            return await _db.CategoryMilitaryEquipments.ToListAsync();
        }

        public async Task<CategoryMilitaryEquipment> GetById(int id)
        {
            return await _db.CategoryMilitaryEquipments.FindAsync(id);
        }

        public void Update(CategoryMilitaryEquipment data)
        {
            _db.Entry(data).State = EntityState.Modified;
            _db.SaveChangesAsync();
        }
    }
}
