﻿using FileCabinet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.AppContext;
using System.Data.Entity;

namespace FileCabinet.DAL.Repositories
{
    public class MilitaryEquipmentRepository : IMilitaryEquipmentRepository<MilitaryEquipment>
    {
        private readonly ApplicationContext _db;

        public MilitaryEquipmentRepository(ApplicationContext context)
        {
            _db = context;
        }

        public void Create(MilitaryEquipment data)
        {
            _db.MilitaryEquipments.Add(data);
            _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            MilitaryEquipment data = await _db.MilitaryEquipments.FindAsync(id);
            if (data != null)
                _db.MilitaryEquipments.Remove(data);

            await _db.SaveChangesAsync();
        }

        public IEnumerable<MilitaryEquipment> Find(Func<MilitaryEquipment, bool> predicate)
        {
            return _db.MilitaryEquipments
                .Include(m => m.CategoryMilitaryEquipment)
                .Include(m => m.Country)
                .Include(m => m.TypeArmy)
                .Where(predicate)
                .ToList();
        }

        public async Task<IEnumerable<MilitaryEquipment>> GetAll()
        {
            return await _db.MilitaryEquipments.Include(m => m.CategoryMilitaryEquipment).Include(m => m.Country).Include(m => m.TypeArmy).ToListAsync();
        }

        public async Task<MilitaryEquipment> GetById(int id)
        {
            return await _db.MilitaryEquipments.FindAsync(id);
        }

        public void Update(MilitaryEquipment data)
        {
            _db.Entry(data).State = EntityState.Modified;
            _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Country>> GetSelectedCountries(int? id)
        {
            return await _db.MilitaryEquipments
                .Include(i => i.Country)
                .Where(i => i.TypeArmyID == id)
                .GroupBy(g => g.Country)
                .Select(f => f.Key).ToListAsync();
        }

        public async Task<IEnumerable<Country>> SelectTypeArmy(int? id)
        {
            return await _db.MilitaryEquipments
                .Include(i => i.Country)
                .Where(i => i.TypeArmyID == id)
                .GroupBy(g => g.Country)
                .Select(f => f.Key).ToListAsync();
        }

        public async Task<IEnumerable<CategoryMilitaryEquipment>> SelectCountryGetCategory(int? CountryID, int? TypeArmyID)
        {
            return await _db.MilitaryEquipments
                .Include(i => i.CategoryMilitaryEquipment)
                .Where(z => z.CountryID == CountryID)
                .Where(z => z.TypeArmyID == TypeArmyID)
                .GroupBy(z => z.CategoryMilitaryEquipment)
                .Select(z => z.Key)
                .ToListAsync();
        }

        public async Task<IEnumerable<MilitaryEquipment>> SelectCategoryGetMilitaryEquipment(int? CategoryID, int? TypeArmyID, int? CountryID)
        {
            return await _db.MilitaryEquipments
                .Include(m => m.CategoryMilitaryEquipment)
                .Include(m => m.Country)
                .Include(m => m.TypeArmy)
                .Where(z => z.CountryID == CountryID)
                .Where(z => z.TypeArmyID == TypeArmyID)
                .Where(z => z.CategoryMilitaryEquipmentID == CategoryID)
                .ToListAsync();
        }

        public IQueryable<MilitaryEquipment> GetAsQueryableResult()
        {
            return _db.MilitaryEquipments
                .Include(m => m.CategoryMilitaryEquipment)
                .Include(m => m.Country)
                .Include(m => m.TypeArmy)
                .AsQueryable();
        }
    }
}
