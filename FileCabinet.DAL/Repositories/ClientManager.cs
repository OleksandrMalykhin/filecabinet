﻿using FileCabinet.DAL.AppContext;
using FileCabinet.DAL.IdentityEntities;
using FileCabinet.DAL.Interfaces;

namespace FileCabinet.DAL.Repositories
{
    public class ClientManager : IClientManager
    {
        public ApplicationContext Database { get; set; }
        public ClientManager(ApplicationContext db)
        {
            Database = db;
        }

        public void Create(UserProfile item)
        {
            Database.ClientProfiles.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
