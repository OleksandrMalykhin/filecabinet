﻿using FileCabinet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.AppContext;
using System.Data.Entity;

namespace FileCabinet.DAL.Repositories
{
    public class ReviewsRepository : IRepository<Reviews>
    {
        private readonly ApplicationContext _db;

        public ReviewsRepository(ApplicationContext context)
        {
            _db = context;
        }

        /// <summary>
        /// Add new review to db
        /// </summary>
        public void Create(Reviews data)
        {
            _db.Reviews.Add(data);
            _db.SaveChanges();

            var rating = _db.Reviews
                .Where(i => i.MilitaryEquipmentID == data.MilitaryEquipmentID)
                .Average(p => p.Score);

            _db.MilitaryEquipments
                .Where(i => i.MilitaryEquipmentID == data.MilitaryEquipmentID)
                .First().Rating = rating;

            _db.SaveChanges();
        }

        /// <summary>
        /// Updates the rating where the user added or changed it
        /// </summary>
        private void UpdateRating(Reviews data)
        {
            var rating = _db.Reviews
                .Where(i => i.MilitaryEquipmentID == data.MilitaryEquipmentID)
                .Average(p => p.Score);

            _db.MilitaryEquipments
                .Where(i => i.MilitaryEquipmentID == data.MilitaryEquipmentID)
                .First().Rating = rating;

            _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            Reviews data = await _db.Reviews.FindAsync(id);
            if (data != null)
                _db.Reviews.Remove(data);
        }

        public IEnumerable<Reviews> Find(Func<Reviews, bool> predicate)
        {
            return _db.Reviews.Where(predicate).ToList();
        }

        public async Task<IEnumerable<Reviews>> GetAll()
        {
            return await _db.Reviews.ToListAsync();
        }

        public async Task<Reviews> GetById(int id)
        {
            return await _db.Reviews.FindAsync(id);
        }

        public void Update(Reviews data)
        {
            _db.Entry(data).State = EntityState.Modified;
            _db.SaveChanges();

            var rating = _db.Reviews
                .Where(i => i.MilitaryEquipmentID == data.MilitaryEquipmentID)
                .Average(p => p.Score);

            _db.MilitaryEquipments
                .Where(i => i.MilitaryEquipmentID == data.MilitaryEquipmentID)
                .First().Rating = rating;

            _db.SaveChanges();
        }
    }
}
