﻿using FileCabinet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.DAL.Entities;
using FileCabinet.DAL.AppContext;
using System.Data.Entity;

namespace FileCabinet.DAL.Repositories
{
    public class TypeArmyRepository : IRepository<TypeArmy>
    {
        private readonly ApplicationContext _db;

        public TypeArmyRepository(ApplicationContext context)
        {
            _db = context;
        }

        public void Create(TypeArmy data)
        {
            _db.TypeArmies.Add(data);
            _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            TypeArmy data = await _db.TypeArmies.FindAsync(id);
            if (data != null)
                _db.TypeArmies.Remove(data);
            await _db.SaveChangesAsync();
        }

        public IEnumerable<TypeArmy> Find(Func<TypeArmy, bool> predicate)
        {
            return _db.TypeArmies.Where(predicate).ToList();
        }

        public async Task<IEnumerable<TypeArmy>> GetAll()
        {
            return await _db.TypeArmies.ToListAsync();
        }

        public async Task<TypeArmy> GetById(int id)
        {
            return await _db.TypeArmies.FindAsync(id);
        }

        public void Update(TypeArmy data)
        {
            _db.Entry(data).State = EntityState.Modified;
            _db.SaveChangesAsync();
        }
    }
}
