﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace FileCabinet.DAL.IdentityEntities
{
    public class ApplicationUser : IdentityUser
    {
        public virtual UserProfile UserProfile { get; set; }
    }
}
