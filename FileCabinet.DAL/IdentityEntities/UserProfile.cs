﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FileCabinet.DAL.IdentityEntities
{
    public class UserProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string UserProfileId { get; set; }

        public string Login { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
