﻿using System.Collections.Generic;

namespace FileCabinet.DAL.Entities
{
    public class Country
    {
        public int CountryID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }

        public ICollection<MilitaryEquipment> MilitaryEquipment { get; set; }
    }
}
