﻿using FileCabinet.DAL.IdentityEntities;

namespace FileCabinet.DAL.Entities
{
    public class Reviews
    {
        public int Score { get; set; }

        public string UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }

        public int MilitaryEquipmentID { get; set; }
        public MilitaryEquipment MilitaryEquipments { get; set; }
    }
}
