﻿namespace FileCabinet.DAL.Entities
{
    public class MilitaryEquipment
    {
        public int MilitaryEquipmentID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public double Rating { get; set; }

        public int CategoryMilitaryEquipmentID { get; set; }
        public CategoryMilitaryEquipment CategoryMilitaryEquipment { get; set; }

        public int CountryID { get; set; }
        public Country Country { get; set; }

        public int TypeArmyID { get; set; }
        public TypeArmy TypeArmy { get; set; }
    }
}
