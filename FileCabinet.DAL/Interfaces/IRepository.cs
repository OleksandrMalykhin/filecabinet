﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileCabinet.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T data);
        void Update(T data);
        Task Delete(int id);
    }
}
