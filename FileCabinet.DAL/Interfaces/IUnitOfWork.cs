﻿using System;
using FileCabinet.DAL.Entities;

namespace FileCabinet.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<CategoryMilitaryEquipment> CategoryMilitaryEquipments { get; }
        IRepository<Country> Countries { get; }
        IMilitaryEquipmentRepository<MilitaryEquipment> MilitaryEquipments { get; }
        IRepository<Reviews> Reviews { get; }
        IRepository<TypeArmy> TypeArmies { get; }

        void Save();
    }
}
