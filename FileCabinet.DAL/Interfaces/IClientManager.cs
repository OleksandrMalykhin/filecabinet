﻿using FileCabinet.DAL.IdentityEntities;
using System;

namespace FileCabinet.DAL.Interfaces
{
    public interface IClientManager : IDisposable
    {
        void Create(UserProfile item);
    }
}
