﻿using FileCabinet.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace FileCabinet.DAL.Interfaces
{
    public interface IIdentitiUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}
