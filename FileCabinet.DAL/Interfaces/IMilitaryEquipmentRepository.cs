﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCabinet.DAL.Entities;

namespace FileCabinet.DAL.Interfaces
{
    public interface IMilitaryEquipmentRepository<T> : IRepository<T> where T : class
    {
        Task<IEnumerable<Country>> SelectTypeArmy(int? id);
        Task<IEnumerable<CategoryMilitaryEquipment>> SelectCountryGetCategory(int? CountryID, int? TypeArmyID);
        Task<IEnumerable<MilitaryEquipment>> SelectCategoryGetMilitaryEquipment(int? CategoryID, int? TypeArmyID, int? CountryID);

        IQueryable<MilitaryEquipment> GetAsQueryableResult();
    }
}
