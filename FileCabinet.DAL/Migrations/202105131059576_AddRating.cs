﻿namespace FileCabinet.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddRating : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reviews", "User_Reviews_UserReviewID", "dbo.UserReviews");
            DropIndex("dbo.Reviews", new[] { "User_Reviews_UserReviewID" });
            RenameColumn(table: "dbo.UserProfiles", name: "Id", newName: "UserProfileId");
            RenameIndex(table: "dbo.UserProfiles", name: "IX_Id", newName: "IX_UserProfileId");
            AddColumn("dbo.MilitaryEquipments", "Rating", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Reviews", "Score", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "UserProfileId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Reviews", "UserProfileId");
            AddForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles", "UserProfileId");
            DropColumn("dbo.Reviews", "UserId");
            DropColumn("dbo.Reviews", "User_Reviews_UserReviewID");
            DropTable("dbo.UserReviews");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserReviews",
                c => new
                    {
                        UserReviewID = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.UserReviewID);
            
            AddColumn("dbo.Reviews", "User_Reviews_UserReviewID", c => c.Int());
            AddColumn("dbo.Reviews", "UserId", c => c.String());
            DropForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles");
            DropIndex("dbo.Reviews", new[] { "UserProfileId" });
            DropColumn("dbo.Reviews", "UserProfileId");
            DropColumn("dbo.Reviews", "Score");
            DropColumn("dbo.MilitaryEquipments", "Rating");
            RenameIndex(table: "dbo.UserProfiles", name: "IX_UserProfileId", newName: "IX_Id");
            RenameColumn(table: "dbo.UserProfiles", name: "UserProfileId", newName: "Id");
            CreateIndex("dbo.Reviews", "User_Reviews_UserReviewID");
            AddForeignKey("dbo.Reviews", "User_Reviews_UserReviewID", "dbo.UserReviews", "UserReviewID");
        }
    }
}
