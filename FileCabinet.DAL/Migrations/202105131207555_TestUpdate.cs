﻿namespace FileCabinet.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class TestUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles");
            DropIndex("dbo.Reviews", new[] { "UserProfileId" });
            DropPrimaryKey("dbo.Reviews");
            AlterColumn("dbo.MilitaryEquipments", "Rating", c => c.Double(nullable: false));
            AlterColumn("dbo.Reviews", "UserProfileId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Reviews", new[] { "MilitaryEquipmentID", "UserProfileId" });
            CreateIndex("dbo.Reviews", "UserProfileId");
            AddForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles", "UserProfileId", cascadeDelete: true);
            DropColumn("dbo.Reviews", "ReviewsID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reviews", "ReviewsID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles");
            DropIndex("dbo.Reviews", new[] { "UserProfileId" });
            DropPrimaryKey("dbo.Reviews");
            AlterColumn("dbo.Reviews", "UserProfileId", c => c.String(maxLength: 128));
            AlterColumn("dbo.MilitaryEquipments", "Rating", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddPrimaryKey("dbo.Reviews", "ReviewsID");
            CreateIndex("dbo.Reviews", "UserProfileId");
            AddForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles", "UserProfileId");
        }
    }
}
