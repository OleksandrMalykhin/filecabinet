﻿namespace FileCabinet.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IntialDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryMilitaryEquipments",
                c => new
                    {
                        CategoryMilitaryEquipmentID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryMilitaryEquipmentID);
            
            CreateTable(
                "dbo.MilitaryEquipments",
                c => new
                    {
                        MilitaryEquipmentID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CategoryMilitaryEquipmentID = c.Int(nullable: false),
                        CountryID = c.Int(nullable: false),
                        TypeArmyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MilitaryEquipmentID)
                .ForeignKey("dbo.CategoryMilitaryEquipments", t => t.CategoryMilitaryEquipmentID, cascadeDelete: true)
                .ForeignKey("dbo.Countries", t => t.CountryID, cascadeDelete: true)
                .ForeignKey("dbo.TypeArmies", t => t.TypeArmyID, cascadeDelete: true)
                .Index(t => t.CategoryMilitaryEquipmentID)
                .Index(t => t.CountryID)
                .Index(t => t.TypeArmyID);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        CountryID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CountryID);
            
            CreateTable(
                "dbo.TypeArmies",
                c => new
                    {
                        TypeArmyID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TypeArmyID);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ReviewsID = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        MilitaryEquipmentID = c.Int(nullable: false),
                        User_Reviews_UserReviewID = c.Int(),
                    })
                .PrimaryKey(t => t.ReviewsID)
                .ForeignKey("dbo.MilitaryEquipments", t => t.MilitaryEquipmentID, cascadeDelete: true)
                .ForeignKey("dbo.UserReviews", t => t.User_Reviews_UserReviewID)
                .Index(t => t.MilitaryEquipmentID)
                .Index(t => t.User_Reviews_UserReviewID);
            
            CreateTable(
                "dbo.UserReviews",
                c => new
                    {
                        UserReviewID = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.UserReviewID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "User_Reviews_UserReviewID", "dbo.UserReviews");
            DropForeignKey("dbo.Reviews", "MilitaryEquipmentID", "dbo.MilitaryEquipments");
            DropForeignKey("dbo.MilitaryEquipments", "TypeArmyID", "dbo.TypeArmies");
            DropForeignKey("dbo.MilitaryEquipments", "CountryID", "dbo.Countries");
            DropForeignKey("dbo.MilitaryEquipments", "CategoryMilitaryEquipmentID", "dbo.CategoryMilitaryEquipments");
            DropIndex("dbo.Reviews", new[] { "User_Reviews_UserReviewID" });
            DropIndex("dbo.Reviews", new[] { "MilitaryEquipmentID" });
            DropIndex("dbo.MilitaryEquipments", new[] { "TypeArmyID" });
            DropIndex("dbo.MilitaryEquipments", new[] { "CountryID" });
            DropIndex("dbo.MilitaryEquipments", new[] { "CategoryMilitaryEquipmentID" });
            DropTable("dbo.UserReviews");
            DropTable("dbo.Reviews");
            DropTable("dbo.TypeArmies");
            DropTable("dbo.Countries");
            DropTable("dbo.MilitaryEquipments");
            DropTable("dbo.CategoryMilitaryEquipments");
        }
    }
}
