﻿namespace FileCabinet.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddImageToEntities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoryMilitaryEquipments", "Image", c => c.Binary());
            AddColumn("dbo.MilitaryEquipments", "Image", c => c.Binary());
            AddColumn("dbo.Countries", "Image", c => c.Binary());
            AddColumn("dbo.TypeArmies", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TypeArmies", "Image");
            DropColumn("dbo.Countries", "Image");
            DropColumn("dbo.MilitaryEquipments", "Image");
            DropColumn("dbo.CategoryMilitaryEquipments", "Image");
        }
    }
}
