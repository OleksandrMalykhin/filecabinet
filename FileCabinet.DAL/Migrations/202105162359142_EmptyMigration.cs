﻿namespace FileCabinet.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EmptyMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "Login", c => c.String());
            DropColumn("dbo.UserProfiles", "Name");
            DropColumn("dbo.UserProfiles", "Address");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "Address", c => c.String());
            AddColumn("dbo.UserProfiles", "Name", c => c.String());
            DropColumn("dbo.UserProfiles", "Login");
        }
    }
}
